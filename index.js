
const express = require('express');
const bodyParser = require('body-parser');
const serverlessHttp = require('serverless-http');
const cors = require('cors');
const { PrismaClient } = require('@prisma/client');
const dotenv = require('dotenv');
const ProductRoute = require('./routes/ProductRoute.js');
const OfferRoute = require('./routes/OfferRoute.js');
const UserRoute = require('./routes/userRoute.js');
const Souscription = require('./routes/souscriptionRoute.js');
const { createServer } = require('http');
const session = require('express-session');


dotenv.config();

const prisma = new PrismaClient();

const app = express();

const Port = process.env.PORT || 3000;

app.use(cors());
app.use(express.urlencoded({ extended: false }));
app.use(express.json());


// Configuration de la session
app.use(session({
  secret: '1234567890abcdefg',
  resave: false,
  saveUninitialized: true
}));

//  les routes
// app.use(ProductRoute);
// app.use(OfferRoute);
// app.use(UserRoute);
// app.use(Souscription);


const Server = createServer(app);

 app.use('/',ProductRoute)
app.use('/', UserRoute);
app.use('/',ProductRoute)
app.use('/', OfferRoute);
app.use('/', Souscription);
// app.use('/api/users', userRouter);



Server.listen( Port,() => {
    console.log(`Server demarrer`);
});

// Export de la fonction handler pour AWS Lambda
module.exports.handler = serverlessHttp(app);
