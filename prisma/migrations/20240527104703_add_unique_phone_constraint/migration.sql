/*
  Warnings:

  - A unique constraint covering the columns `[phone]` on the table `Souscripteur` will be added. If there are existing duplicate values, this will fail.

*/
-- CreateIndex
CREATE UNIQUE INDEX `Souscripteur_phone_key` ON `Souscripteur`(`phone`);
