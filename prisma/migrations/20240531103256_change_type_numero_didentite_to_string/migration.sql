/*
  Warnings:

  - Made the column `numero_identite` on table `Souscripteur` required. This step will fail if there are existing NULL values in that column.

*/
-- AlterTable
ALTER TABLE `Souscripteur` MODIFY `numero_identite` VARCHAR(191) NOT NULL;
