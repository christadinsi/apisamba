-- CreateTable
CREATE TABLE `Souscripteur` (
    `idSouscripteur` INTEGER NOT NULL AUTO_INCREMENT,
    `first_name` VARCHAR(191) NOT NULL,
    `last_name` VARCHAR(191) NOT NULL,
    `date_born` DATETIME(3) NOT NULL,
    `numero_identite` INTEGER NULL,
    `bp` VARCHAR(191) NULL,
    `address` VARCHAR(191) NULL,
    `city` VARCHAR(191) NULL,
    `phone` VARCHAR(191) NULL,
    `email` VARCHAR(191) NULL,
    `createdAt` DATETIME(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),

    PRIMARY KEY (`idSouscripteur`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `Souscription` (
    `idSouscription` INTEGER NOT NULL AUTO_INCREMENT,
    `createdAt` DATETIME(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
    `subscriberId` INTEGER NOT NULL,
    `assured_first_name` VARCHAR(191) NOT NULL,
    `assured_last_name` VARCHAR(191) NOT NULL,
    `assured_date_born` DATETIME(3) NOT NULL,
    `assured_address` VARCHAR(191) NULL,
    `assured_city` VARCHAR(191) NULL,
    `assured_bp` VARCHAR(191) NULL,
    `assured_phone` INTEGER NULL,
    `assured_email` VARCHAR(191) NULL,
    `numero_police` INTEGER NOT NULL,
    `date_effet` DATETIME(3) NOT NULL,
    `date_echeance` DATETIME(3) NOT NULL,
    `contact_encas_besoin` VARCHAR(191) NOT NULL,
    `tel_Contact_encas_besoin` INTEGER NOT NULL,

    PRIMARY KEY (`idSouscription`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `Contract` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `createdAt` DATETIME(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
    `subscriptionId` INTEGER NOT NULL,
    `offerId` INTEGER NOT NULL,

    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `Offer` (
    `idOffre` INTEGER NOT NULL AUTO_INCREMENT,
    `type` VARCHAR(191) NOT NULL,
    `titre` VARCHAR(191) NOT NULL,
    `description` VARCHAR(191) NOT NULL,
    `image` VARCHAR(191) NOT NULL,
    `createdAt` DATETIME(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),

    PRIMARY KEY (`idOffre`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `Cotisation` (
    `idCotisation` INTEGER NOT NULL AUTO_INCREMENT,
    `periodicite` VARCHAR(191) NOT NULL,
    `montant` DOUBLE NOT NULL,
    `subscriptionId` INTEGER NOT NULL,

    PRIMARY KEY (`idCotisation`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `Garantie` (
    `idGarantie` INTEGER NOT NULL AUTO_INCREMENT,
    `type` VARCHAR(191) NOT NULL,
    `Description` VARCHAR(191) NOT NULL,
    `offerId` INTEGER NOT NULL,

    PRIMARY KEY (`idGarantie`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `Periodicite` (
    `idPeriodicite` INTEGER NOT NULL AUTO_INCREMENT,
    `type` VARCHAR(191) NOT NULL,
    `montant` DOUBLE NOT NULL,
    `offerId` INTEGER NOT NULL,

    PRIMARY KEY (`idPeriodicite`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- AddForeignKey
ALTER TABLE `Souscription` ADD CONSTRAINT `Souscription_subscriberId_fkey` FOREIGN KEY (`subscriberId`) REFERENCES `Souscripteur`(`idSouscripteur`) ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `Contract` ADD CONSTRAINT `Contract_subscriptionId_fkey` FOREIGN KEY (`subscriptionId`) REFERENCES `Souscription`(`idSouscription`) ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `Contract` ADD CONSTRAINT `Contract_offerId_fkey` FOREIGN KEY (`offerId`) REFERENCES `Offer`(`idOffre`) ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `Cotisation` ADD CONSTRAINT `Cotisation_subscriptionId_fkey` FOREIGN KEY (`subscriptionId`) REFERENCES `Souscription`(`idSouscription`) ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `Garantie` ADD CONSTRAINT `Garantie_offerId_fkey` FOREIGN KEY (`offerId`) REFERENCES `Offer`(`idOffre`) ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `Periodicite` ADD CONSTRAINT `Periodicite_offerId_fkey` FOREIGN KEY (`offerId`) REFERENCES `Offer`(`idOffre`) ON DELETE RESTRICT ON UPDATE CASCADE;
