/*
  Warnings:

  - You are about to alter the column `date_born` on the `Souscripteur` table. The data in that column could be lost. The data in that column will be cast from `VarChar(191)` to `DateTime(3)`.

*/
-- AlterTable
ALTER TABLE `Souscripteur` MODIFY `date_born` DATETIME(3) NOT NULL;
