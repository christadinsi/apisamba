/*
  Warnings:

  - Made the column `phone` on table `Souscripteur` required. This step will fail if there are existing NULL values in that column.

*/
-- AlterTable
ALTER TABLE `Souscripteur` MODIFY `phone` VARCHAR(191) NOT NULL;
