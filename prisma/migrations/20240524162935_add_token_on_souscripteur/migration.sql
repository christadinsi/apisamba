/*
  Warnings:

  - You are about to alter the column `phone` on the `Souscripteur` table. The data in that column could be lost. The data in that column will be cast from `VarChar(191)` to `Int`.

*/
-- AlterTable
ALTER TABLE `Souscripteur` ADD COLUMN `accessToken` VARCHAR(191) NULL,
    MODIFY `phone` INTEGER NULL;
