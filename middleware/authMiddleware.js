const jwt = require('jsonwebtoken');
const secretKey = '1234567890abcdefg';

const authMiddleware = (req, res, next) => {
  const authHeader = req.headers.authorization;

  if (!authHeader) {
    return res.status(401).json({ message: 'Aucun jeton d\'accès fourni.' });
  }

  const token = authHeader.split(' ')[1];

  jwt.verify(token, secretKey, (err, decoded) => {
    if (err) {
      return res.status(403).json({ message: 'Jeton d\'accès invalide.' });
    }

    req.user = { userId: decoded.userId };
    next();
  });
};

module.exports = {
  authMiddleware,
};
