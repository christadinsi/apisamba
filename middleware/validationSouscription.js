// validationMiddleware.js

const { body } = require('express-validator');

const validateSouscripteurData = [
    body('first_name').notEmpty().withMessage('Le prénom est requis'),
    body('last_name').notEmpty().withMessage('Le nom de famille est requis'),
    body('date_born').isISO8601().toDate().withMessage('La date de naissance est requise et doit être au format ISO 8601'),
    body('numero_identite').notEmpty().withMessage('Le numéro d\'identité est requis'),
    body('bp').notEmpty().withMessage('Le BP est requis'),
    body('address').notEmpty().withMessage('L\'adresse est requise'),
    body('city').notEmpty().withMessage('La ville est requise'),
    body('phone').notEmpty().withMessage('Le numéro de portable est requis'),
    body('email').isEmail().withMessage('L\'email est requis et doit être au format email'),
];

module.exports = {
    validateSouscripteurData,
};
