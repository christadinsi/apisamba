const express = require('express');

const {
    getOffers,
    getOffer,
    createOffer,
    updateOffer,
    deleteOffer } = require('../controllers/OfferControllers.js');

const router = express.Router();

// Routes pour les opérations CRUD sur les offres
router.get('/offers', getOffers);
router.get('/offers/:idOffre', getOffer); 
router.post('/offers', createOffer); 
router.put('/offers/:idOffre', updateOffer); 
router.delete('/offers/:idOffre', deleteOffer);

module.exports = router;