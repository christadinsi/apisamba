const express = require('express');
const router = express.Router();
const souscriptionController = require('../controllers/souscriptionControllers.js');

// Route pour sélectionner tous les souscriptions
router.get('/souscriptions', souscriptionController.getAllSouscriptions);

// Route pour sélectionner un souscription par ID
router.get('/souscriptions/:id', souscriptionController.getSouscriptionById);

// Route pour créer un nouveau souscription
router.post('/souscriptions', souscriptionController.createSouscription);

// Route pour mettre à jour un souscription
router.put('/souscriptions/:id', souscriptionController.updateSouscription);

// Route pour supprimer un souscription
router.delete('/souscriptions/:id', souscriptionController.deleteSouscription);


module.exports = router;