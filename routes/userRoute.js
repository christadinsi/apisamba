const express = require('express');
const router = express.Router();
const souscripteurController = require('../controllers/userController.js');

// Route pour créer un nouveau souscripteur
router.post('/souscripteur', souscripteurController.createSouscripteur);

// Route pour récupérer tous les souscripteurs
router.get('/souscripteurs', souscripteurController.getAllSouscripteurs);

// Route pour récupérer un souscripteur par ID
router.get('/souscripteurs/:id', souscripteurController.getSouscripteurById);

// Route pour mettre à jour un souscripteur
router.put('/souscripteurs/:id', souscripteurController.updateSouscripteur);

// Route pour supprimer un souscripteur
router.delete('/souscripteurs/:id', souscripteurController.deleteSouscripteur); 

// Route pour se connecter
router.post('/login', souscripteurController.login);

// Route pour se connecter
router.post('/logout', souscripteurController.logout);

module.exports = router;