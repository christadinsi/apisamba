const express = require('express');

const { 
    getProducts, 
    getProductBy, 
    createProduct, 
    updateProduct, 
    deleteProduct } = require("../controllers/ProductControllers.js");

const router = express.Router();

router.get("/products", getProducts);
router.get("/products/:id", getProductBy);
router.post("/products", createProduct);
router.patch("/products/:id", updateProduct);
router.delete("/products/:id", deleteProduct);

module.exports = router;
