const { PrismaClient } = require('@prisma/client');
const prisma = new PrismaClient();
const jwt = require('jsonwebtoken'); // Importer le module jsonwebtoken
const { secretKey } = require('../config');



// Créer un nouveau souscripteur
const createSouscripteur = async (req, res) => {
    const { first_name, last_name, date_born, numero_identite, bp, address, city, phone, email } = req.body;

    try {
        const newSouscripteur = await prisma.souscripteur.create({
            data: {
                first_name,
                last_name,
                date_born: new Date(date_born),
                numero_identite,
                bp,
                address,
                city,
                phone,
                email,
            },
            include: {
                souscriptions: true,
            },
        });
        res.status(201).json(newSouscripteur);
    } catch (error) {
        res.status(500).json({ message: error.message });
    }
};

// Obtenir tous les souscripteurs
const getAllSouscripteurs = async (req, res) => {
    try {
        const souscripteurs = await prisma.souscripteur.findMany({
            include: {
                souscriptions: true,
            },
        });
        res.status(200).json(souscripteurs);
    } catch (error) {
        res.status(500).json({ message: error.message });
    }
};

// Obtenir un souscripteur par ID
const getSouscripteurById = async (req, res) => {
    const { id } = req.params;

    try {
        const souscripteur = await prisma.souscripteur.findUnique({
            where: {
                idSouscripteur: parseInt(id),
            },
            include: {
                souscriptions: true,
            },
        });
        if (!souscripteur) {
            return res.status(404).json({ message: 'Souscripteur not found' });
        }
        res.status(200).json(souscripteur);
    } catch (error) {
        res.status(500).json({ message: error.message });
    }
};

// Mettre à jour un souscripteur
const updateSouscripteur = async (req, res) => {
    const { id } = req.params;
    const { first_name, last_name, date_born, numero_identite, bp, address, city, phone, email } = req.body;

    try {
        const updatedSouscripteur = await prisma.souscripteur.update({
            where: {
                idSouscripteur: parseInt(id),
            },
            data: {
                first_name,
                last_name,
                date_born: new Date(date_born),
                numero_identite,
                bp,
                address,
                city,
                phone,
                email,
            },
            include: {
                souscriptions: true,
            },
        });
        res.status(200).json(updatedSouscripteur);
    } catch (error) {
        res.status(500).json({ message: error.message });
    }
};

// Supprimer un souscripteur
const deleteSouscripteur = async (req, res) => {
    const { id } = req.params;

    try {
        await prisma.souscripteur.delete({
            where: {
                idSouscripteur: parseInt(id),
            },
        });
        res.status(200).json({ message: 'Souscripteur deleted successfully' });
    } catch (error) {
        res.status(500).json({ message: error.message });
    }
};

const login = async (req, res) => {
  const { phone } = req.body;

  try {
    // Vérifier si le numéro de téléphone est fourni
    if (!phone) {
      return res.status(400).json({ message: 'Veuillez fournir un numéro de téléphone.' });
    }

    // Trouver l'utilisateur avec le numéro de téléphone fourni
      const user = await prisma.souscripteur.findUnique({
          where: { phone }
      });
      

    // Vérifier si l'utilisateur existe
    if (!user) {
      return res.status(404).json({ message: 'Aucun utilisateur trouvé avec ce numéro de téléphone.' });
    }

// Vérifier si l'utilisateur a déjà un jeton d'accès valide
    const now = new Date();
    const accessToken = user.accessToken;
    const accessTokenExpiry = user.accessTokenExpiry;

    if (accessToken && accessTokenExpiry && accessTokenExpiry.getTime() > now.getTime()) {
      // Le jeton d'accès est valide, renvoyer le jeton d'accès existant
      return res.status(200).json({ message: 'Connexion réussie', accessToken });
    }

    // Générer un nouveau jeton d'accès pour l'utilisateur
    const newAccessToken = jwt.sign({ userId: user.idSouscripteur }, secretKey, { expiresIn: '1h' });

    // Mettre à jour le jeton d'accès de l'utilisateur en base de données
    await prisma.souscripteur.update({
      where: { idSouscripteur: user.idSouscripteur },
      data: { accessToken: newAccessToken, accessTokenExpiry: new Date(Date.now() + 3600000) } // expire dans 1 heure
    });


    // Renvoyer une réponse JSON avec un message de succès et le jeton d'accès
    return res.status(200).json({ message: 'Connexion réussie', accessToken });
  } catch (error) {
    console.error('Erreur lors de la connexion :', error);
    return res.status(500).json({ message: 'Erreur serveur lors de la connexion.' });
  }
};


const logout = (req, res) => {
  // Supprimer la session de l'utilisateur
    req.session.destroy();
    
   // Renvoie une réponse JSON avec un message de succès
  res.json({
    success: true,
    message: 'Déconnexion réussie',
  });
};

module.exports = {
    createSouscripteur,
    getAllSouscripteurs,
    getSouscripteurById,
    updateSouscripteur,
    deleteSouscripteur,
    login,
    logout
};