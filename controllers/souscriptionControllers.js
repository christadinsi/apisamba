const { PrismaClient } = require('@prisma/client');
const prisma = new PrismaClient();

const createSouscription = async (req, res) => {
  const { subscriberId, assured_first_name, assured_last_name, assured_date_born, assured_address, assured_bp, assured_phone, numero_police, date_effet, date_echeance, contact_encas_besoin, tel_Contact_encas_besoin } = req.body;

  try {
    // Vérifier si le souscripteur existe
    const subscriber = await prisma.souscripteur.findUnique({
      where: {
        idSouscripteur: subscriberId
      }
    });

    if (!subscriber) {
      return res.status(404).json({ message: 'Aucun souscripteur trouvé avec cet identifiant.' });
    }

    // Vérifier si la date d'échéance est après la date d'effet
    if (new Date(date_echeance) <= new Date(date_effet)) {
      return res.status(400).json({ message: 'La date d\'échéance doit être après la date d\'effet.' });
    }

    // Créer la nouvelle souscription
    const newSouscription = await prisma.souscription.create({
      data: {
        subscriberId,
        assured_first_name,
        assured_last_name,
        assured_date_born: new Date(assured_date_born),
        assured_address,
        assured_bp,
        assured_phone,
        numero_police,
        date_effet: new Date(date_effet),
        date_echeance: new Date(date_echeance),
        contact_encas_besoin,
        tel_Contact_encas_besoin
      },
      include: {
        subscriber: true,
        cotisations: true,
        contracts: true
      }
    });

    res.status(201).json(newSouscription);
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: 'Erreur serveur lors de la création de la souscription.' });
  }
};



const getAllSouscriptions = async (req, res) => {
    try {
        const souscriptions = await prisma.souscription.findMany({
            include: {
                souscripteur: true,
                offer: true,
            },
        });
        res.status(200).json(souscriptions);
    } catch (error) {
        res.status(500).json({ message: error.message });
    }
};

const getSouscriptionById = async (req, res) => {
    const { id } = req.params;

    try {
        const souscription = await prisma.souscription.findUnique({
            where: {
                idSouscription: parseInt(id),
            },
            include: {
                souscripteur: true,
                offer: true,
            },
        });
        if (!souscription) {
            return res.status(404).json({ message: 'Souscription not found' });
        }
        res.status(200).json(souscription);
    } catch (error) {
        res.status(500).json({ message: error.message });
    }
};

const updateSouscription = async (req, res) => {
    const { id } = req.params;
    const { subscriberId, offerId } = req.body;

    try {
        const updatedSouscription = await prisma.souscription.update({
            where: {
                idSouscription: parseInt(id),
            },
            data: {
                subscriberId,
                offerId,
            },
            include: {
                souscripteur: true,
                offer: true,
            },
        });
        res.status(200).json(updatedSouscription);
    } catch (error) {
        res.status(500).json({ message: error.message });
    }
};

const deleteSouscription = async (req, res) => {
    const { id } = req.params;

    try {
        await prisma.souscription.delete({
            where: {
                idSouscription: parseInt(id),
            },
        });
        res.status(200).json({ message: 'Souscription deleted successfully' });
    } catch (error) {
        res.status(500).json({ message: error.message });
    }
};

module.exports = {
    createSouscription,
    getAllSouscriptions,
    getSouscriptionById,
    updateSouscription,
    deleteSouscription,
}