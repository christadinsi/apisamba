const { PrismaClient } = require('@prisma/client');
const prisma = new PrismaClient();


// Récupérer toutes les offres
const getOffers = async (req, res) => {
    try {
        const offers = await prisma.offer.findMany();
        res.status(200).json(offers);
    } catch (error) {
        res.status(500).json({ error: 'Erreur lors de la création de l\'offre' });
    }
};
// Créer une nouvelle offre
const createOffer = async (req, res) => {
    const { 
        type,titre, description, colorOffer, image, createdAt } = req.body;

    try {
        const offer = await prisma.offer.create({
            data: {
                type,
                titre,
                description,
                colorOffer,
                image,
                createdAt
            }
        });
        res.status(201).json(offer);
    } catch (error) {
        res.status(400).json({ message: error.message });
    }
};

// Récupérer une offre spécifique
const getOffer = async (req, res) => {
  try {
    const offer = await prisma.offer.findUnique({
      where: { 
        idOffre: Number(req.params.idOffre)
       }
    });
     res.status(200).json(offer);
  } catch (error) {
    res.status(500).json({ error: 'Erreur lors de la récupération de l\'offre' });
  }
};

// Mettre à jour une offre existante
const updateOffer = async (req, res) => {
  const { type, titre, description, colorOffer, image, createdAt} = req.body;
  try {
    const updatedOffer = await prisma.offer.update({
      where: { 
        idOffre: Number(req.params.idOffre)
      },
      data: {
        type: type,
        titre: titre,
        description: description,
        colorOffer: colorOffer,
        image: image,
        createdAt: createdAt
       
      }
    });
    res.status(200).json(updatedOffer);
  } catch (error) {
    res.status(500).json({ error: 'Erreur lors de la mise à jour de l\'offre' });
  }
};

// Supprimer une offre
const deleteOffer = async (req, res) => {
  try {
     const offerId = await prisma.offer.delete({
      where: { 
        idOffre: Number(req.params.idOffre)
      }
    });
    res.status(200).json(offerId);
  } catch (error) {
    res.status(500).json({ error: 'Erreur lors de la suppression de l\'offre' });
  }
};

module.exports = {
  getOffers,
  createOffer,
  getOffer,
  updateOffer,
  deleteOffer
};
